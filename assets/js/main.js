/**
Visionmax v 1.0
Available on ThemeForest - http://themeforest.net/user/nasirwd/portfolio
Author: NasirWd - http://www.nasfactor.com
**/

/*global $:false */
(function(){
 
  "use strict";
    $(window).load(function(){
      $("#nav-sticky").sticky({ topSpacing: 0 });
    });
/* home page soild features animation*/
$(window).scroll(function(){ // scroll event 
	var windowTop = $(window).scrollTop(), // returns number
	solid_action = $('.solid-visuals').offset().top-500;
	if (windowTop >= solid_action)
	{
		$('.solid-iphone,.solid-imac').addClass('animated fadeInUp');
	}

});

  /*smooth scrolling*/

  $(document).ready(function() {
  
//    $("html").niceScroll();  // The document page (html)
    
    $("body").niceScroll({touchbehavior:false,cursorcolor:"#000",cursoropacitymax:1,cursorwidth:8,background:"#333",autohidemode:true});
	
  });

$(function(){var nice=$(":nicescroll").getNiceScroll(0);$("#div1").html($("#div1").html()+' '+nice.version+' ($:'+$().jquery+')')})


  /*home page showcase javascript*/
$(function(){
		
        $(window).on("resize", function () {

		var etwrapper_height= $('.show-case-visual img').height();
		$('.et-wrapper').height(etwrapper_height);
		
		var etwrapper_basic_height= $('.folio-visual').height();
		$('.et-wrapper-basic').height(etwrapper_basic_height+100);
		
        var showcase_hover= $('.show-case-visual img').width();
        
         $('.show-case-visual').width(showcase_hover);
        $('.showcase-hover').width(showcase_hover-5);
          
        var $container = $('.masonry-container');
        $container.masonry({
          itemSelector: '.post-unit'
        });

      }).resize();

	/*flicker plugin*/
	
	$('.flicker').jflickrfeed({
		limit: 6,
		qstrings: {
			/*add your id below*/
			id: '52617155@N08'
		},
		itemTemplate: '<li><a href="{{image_b}}"><img alt="{{title}}" src="{{image_s}}" /></a></li>'
	});
	
	/*twitter plugin*/
	$('#tweets').tweetable({
		/*add your username below*/
		username: 'envato', 
		time: true,
		rotate: true,
		speed: 4000, 
		limit: 4,
		replies: false,
		position: 'append',
		loading: 'loading...' ,
		failed: "Sorry, twitter is currently unavailable for this user.",
		html5: true
	});


	/*testimonial slider*/
	$('.testi-inner').flexslider({
            animation: "slide",
            directionNav: false,
            controlNav: true,
            pauseOnHover: true, 
            slideshow: false,
            direction: "horizontal", //Direction of slides
	});

/*Flex blog slider*/

  $('.blog-slider').flexslider({
    animation: "slide",
	controlNav: false,
  });
  
/*Flex slider*/
  
  $('.flexslider').flexslider({
    animation: "slide"
  });
  
/* Light Box*/
$(document).ready(function() {
  $('.image-link').magnificPopup({type:'image'});
});


/*footer icons*/ 

	$('.head-icon').hover(function(){
		$(this).addClass('animated tada');
	},function(){
		$(this).removeClass('animated tada');
	});
	});  
})();